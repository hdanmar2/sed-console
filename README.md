# sed-console

Basic sed command line

linux:
- Create link to project: sudo npm link
- Delete lint to project: sudo npm unlink 

Information:
sed-console [-option optionValue] "s/regex/replacement/[p|g|i|]" pathInputFile

Options:
      --help     Show help -- Allowed flags: p,g,i --                 [booleano]
      --version  Show version                                         [booleano]
  -n             Suppresses automatic display of result               [booleano]
  -i             Show result as a copy file                           [booleano]
  -e             Add an element to commands list: -e
                 "s/regex/replacement/[p|g|i|]"                          [Array]
  -f             Load commands from file: -f pathFile     [string]