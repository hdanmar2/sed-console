#!/usr/bin/env node
"use strict";
const fs = require("fs");
const path = require("path");
const yargs = require("yargs");

// invoke main function
main();



/*
 * Function Name: main
 * Params: Empty
 * Description: 
 *    Main function
 */
function main() {
  const args = yargs
    .options({
      n: {
        describe: "Suppresses automatic display of result",
        nargs: 0,
        type: "boolean",
      },
      i: {
        describe: "Show result as a copy file",
        nargs: 0,
        type: "boolean",
      },
      e: {
        describe:
          'Add an element to commands list: -e "s/regex/replacement/[p|g|i|]"',
        nargs: 1,
        type: "string",
        array: true,
      },
      f: {
        describe: "Load commands from file: -f pathFile",
        nargs: 1,
        type: "string",
      },
    })
    .describe("help", "Show help -- Allowed flags: p,g,i --")
    .describe("version", "Show version")
    .usage(
      '$0 -[-option optionValue] "s/regex/replacement/[p|g|i|]" pathInputFile'
    ).argv;

  let commands = [];
  let inputFile = "";

  if ((args._.length == 2 && !args.e) || (args._.length == 1 && (!!args.e || !!args.f))) {
    // get non-option parameters
    if (args._.length == 2) {
      commands.push(args._[0]);
      inputFile = args._[1];
    } else {
      inputFile = args._[0];
    }

    fs.access(inputFile, fs.F_OK, (err) => {
      try {
        if (err) {
          //Input file Parameter does not exists end program
          console.error(err);
          return;
        }

        // many commands at the command line
        commands = !!args.e ? args.e : commands;

        // fill commands from a file
        if (!!args.f) {
          // get commands
          fs.readFile(args.f, "utf8", (err, commandsFile) => {
            if (err) {
              console.error(err);
              return;
            }
            // append commands file to commands
            if (commandsFile != "") {
              commands = commandsFile.trim(" ").split("\n").concat(commands);

              if (commands.length > 0) {
                
                //validate commands 
                getCommandObjects(commands)
                .then((arrComands) => {
                  if (!!arrComands) {
                    //get data from input file
                    replaceCommands(
                      !!args.n,
                      arrComands,
                      inputFile,
                      (er, data) => {
                        if (er) {
                          console.error(er);
                          return;
                        }

                        showResult(data, !!args.i, inputFile);
                        return;
                      }
                    );
                  }

                });

              } else {
                // Commands Not found
                console.error(
                  new ExceptionContext(
                    "Invalid Format Exception",
                    "Insufficient commands entered from file"
                  )
                );
              }
            } else {
              throw new ExceptionContext(
                "Invalid Commands Exception",
                "Empty Commands File"
              );
            }
          });
        } else {
          if (commands.length > 0) {
            
            //validate commands
            getCommandObjects(commands)
            .then((arrComands) => {
              if (!!arrComands) {
                //get data from input file
                replaceCommands(!!args.n, arrComands, inputFile, (er, data) => {
                  if (er) {
                    console.error(er);
                    return;
                  }

                  showResult(data, !!args.i, inputFile);
                  return;
                });
              }

            });

          } else {
            // Commands Not found
            console.error(
              new ExceptionContext(
                "Invalid Format Exception",
                "Commands Not Found"
              )
            );
          }
        }
      } catch (error) {
        console.error(error);
        return;
      }
    });
  } else {
    console.error(
      new ExceptionContext(
        "Invalid Format Exception",
        "Invalid Format Parameters"
      )
    );
  }
}

/*
 * Function Name: showResult
 * Params: data string, ioption boolean, inputFile string
 * Description: 
 *    Show data to standard output or print as a copy file.
 * 
 *    Implements i option 
 */
function showResult(data, ioption, inputFile) {
  let extension = path.extname(inputFile);
  let name = path.basename(inputFile, extension) + " (copy)" + extension;
  let dir = path.dirname(inputFile);

  let format = { dir: dir, base: name };

  if (!!ioption) {
    // Output as a copy
    fs.appendFile(path.format(format), data, function (err) {
      if (err) {
        console.error(err);
        return;
      } else {
        console.log("success");
        return;
      }
    });
  } else {
    console.log(data);
  }
}
/*
 * Function Name: replaceCommands
 * Params: ncommand boolean, arrCommands string[], inputFile string, callback Function
 * Description: 
 *    Read the data from the input file, replace the commands in the text 
 *    and return the result to the callback function.
 * 
 *    Implements p flag and n option 
 */
async function replaceCommands(ncommand, arrCommands, inputFile, callback) {
  fs.readFile(inputFile, "utf8", (err, inputData) => {
    let data = inputData;

    try {
      if (data) {
        arrCommands.map((cmd) => {
          let flags = cmd.flags;
          // Start: "n" command's implementation
          if (ncommand) {
            // Start: "p" flag's implementation
            if (flags.search(/[p]/g) >= 0) {
              flags = flags.replace(/[p]/g, "");
              data = data.replace(
                new RegExp(cmd.regexp, flags),
                cmd.replacement
              );
            }
            // End: "p" flag's implementation
          } else {
            flags = flags.replace(/[p]/g, "");
            data = data.replace(new RegExp(cmd.regexp, flags), cmd.replacement);
          }
          // End: "n" command's implementation
        });
      } else {
        throw new ExceptionContext("Empty Content Exception", err);
      }

      return callback(null, data);
    } catch (error) {
      return callback(error, "");
    }
  });
}
/*
 * Function Name: getCommandObjects
 * Params: commandsArr string[]
 * Description: 
 *    Validate commands format and return an object's array
 */
async function getCommandObjects(commandsArr) {
  const duplicatePattern = /(\w)(?=.+\1)/g;
  const notFlagsPattern = /[^g,i,p,]/g;
  let valid = true;
  let cmdObjectsArr = [];
  try {
    await commandsArr.map((command) => {
      const parts = command.trim(" ").split("/");

      if (valid) {
        if (parts.length == 4) {
          if (
            parts[0] == "s" &&
            (parts[3].search(notFlagsPattern) == -1 || parts[3] == "")
          ) {
            let flags = parts[3];

            if (flags.search(duplicatePattern) < 0) {
              cmdObjectsArr.push({
                s_cmd: parts[0] == "s",
                regexp: parts[1],
                replacement: parts[2],
                flags: parts[3],
              });
            } else {
              valid = false;
              throw new ExceptionContext(
                "Invalid Format Exception",
                "There should be no repeated flags"
              );
            }
          } else {
            valid = false;
            throw new ExceptionContext(
              "Invalid Format Exception",
              "Flag not allowed"
            );
          }
        } else {
          valid = false;
          throw new ExceptionContext(
            "Invalid Format Exception",
            "Wrong command"
          );
        }
      }
    });

    cmdObjectsArr = valid ? cmdObjectsArr : [];
  } catch (error) {
    console.error(error);
    return;
  }
  return cmdObjectsArr;
}

function ExceptionContext(title, message) {
  this.title = title;
  this.message = message;
}
